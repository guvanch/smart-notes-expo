import * as React from 'react';
import { useEffect } from 'react'
import Route from './src/route'
import ThemeManager from './src/themes'
import { useFonts, Montserrat_400Regular, Montserrat_600SemiBold, Montserrat_400Regular_Italic } from '@expo-google-fonts/montserrat';
import AppLoading from 'expo-app-loading';
import { createNewTable } from './src/service'

export default function App() {

  let [fontsLoaded] = useFonts({
    Montserrat_400Regular,
    Montserrat_600SemiBold,
    Montserrat_400Regular_Italic
  });

  useEffect(() => {
    async function create() {
      await createNewTable()
    }
    create()
  }, [])


  if (!fontsLoaded) {
    return <AppLoading />;
  } else {
    return (
      <ThemeManager>
        <Route />
      </ThemeManager>
    );
  }
}
