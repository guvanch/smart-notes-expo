<<<<<<< README.md
# Smart Notes

> Smart Notes app build with React Native Expo

## Build Setup

``` bash
# install dependencies
$ yarn or npm install

```

## Run the project

``` bash
# run the project
$ yarn start

# run on IOS device
$ yarn ios

# run on Android device
$ yarn android

```

## Test

``` bash
$ yarn test

```