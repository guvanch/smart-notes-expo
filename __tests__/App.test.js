import React from 'react';
import { create, act } from 'react-test-renderer';
import App from '../App'

test('renders correctly', () => {
  const tree = create(<App />).toJSON();
  expect(tree).toMatchSnapshot();
})
