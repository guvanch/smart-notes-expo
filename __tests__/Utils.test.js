import Dates from '../src/utils/dates';

describe('TIME SINCE', () => {
  it('should return just now', () => {
    var now = new Date()
    expect(Dates.timeSince(now.toString())).toBe('just now')
  })

  it('should return minutes ago', () => {
    var now = new Date()
    var ago = now.getTime() - (1.5 * 60 * 1000)
    expect(Dates.timeSince(ago.toString())).toBe('1 minutes ago')
  })

  it('should return hours ago', () => {
    var now = new Date()
    var ago = now.getTime() - (2 * 3600 * 1000)
    expect(Dates.timeSince(ago.toString())).toBe('2 hours ago')
  })

  it('should return days ago', () => {
    var now = new Date()
    var ago = now.getTime() - (3 * 86400 * 1000)
    expect(Dates.timeSince(ago.toString())).toBe('3 days ago')
  })

  it('should return months ago', () => {
    var now = new Date()
    var ago = now.getTime() - (3 * 2592000 * 1000)
    expect(Dates.timeSince(ago.toString())).toBe('3 months ago')
  })

  it('should return years ago', () => {
    var now = new Date()
    var ago = now.getTime() - (1.5 * 31536000 * 1000)
    expect(Dates.timeSince(ago.toString())).toBe('1 years ago')
  })
})

describe('CURRENT DATE', () => {
  it('match the date format', () => {
    var now = new Date()
    var time = now.getTime()
    expect(Dates.currentDate(time.toString())).toMatch(/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/)
  })
})
