import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { StyledText } from './Styled'
import { useTheme } from '../themes'
import { FontAwesome } from '@expo/vector-icons';

const NoteFound = () => {
  const theme = useTheme()
  return (
    <View style={styles.column}>
      <FontAwesome name='info-circle' size={30} color={theme.mode == 'dark' ? 'white' : 'black'} style={{ marginHorizontal: 10 }} />
      <StyledText style={{ fontSize: 20 }}>Empty</StyledText>
    </View>
  );
};

const styles = StyleSheet.create({
  column: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    top: 0
  }
});

export default NoteFound;
