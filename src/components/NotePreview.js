import * as React from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';
import Dates from '../utils/dates'
import { StyledTitle, StyledBody, StyledTime } from './Styled'
import { useTheme } from '../themes'
import { FontAwesome } from '@expo/vector-icons';

const NotePreview = ({ props }) => {
  const theme = useTheme()
  const item = props
  return (
    <View style={styles.column}>
      <StyledTitle numberOfLines={3} ellipsizeMode='tail'>{item.title}</StyledTitle>
      <View style={styles.row}>
        {item.is_favorite == 1 ?
          <FontAwesome name='star' size={14} color={'#FFBF00'} style={{ marginRight: 10 }} />
          : <View />
        }

        <StyledTime>{Dates.timeSince(item.updated_at)}</StyledTime>
        <View style={{ paddingLeft: 10, flex: 1 }}>
          <StyledBody numberOfLines={1} ellipsizeMode={'tail'}>{item.body}</StyledBody>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  column: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    alignContent: 'stretch',
    flexWrap: 'nowrap',
    padding: 10,
    margin: 0,
    borderBottomWidth: 0.5,
    borderBottomColor: '#bfbfbf',
    borderRadius: 6
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    padding: 6

  },
  headerRight: {
    marginHorizontal: 10
  },
  floatingButton: {
    position: 'absolute',
    bottom: 20,
    right: 20,
  }
});

export default NotePreview;
