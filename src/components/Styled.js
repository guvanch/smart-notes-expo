import styled from 'styled-components/native'

export const StyledView = styled.View`
  flex: 1;
  padding-horizontal: 16px;
  background-color: ${props => props.theme.backgroundAlt};
`

export const StyledText = styled.Text`
  font-size: 18px;
  font-family: 'Montserrat_400Regular'
  color: ${props => props.theme.text};
`

export const StyledTitle = styled.Text`
  font-size: 18px;
  font-family: 'Montserrat_600SemiBold';
  color: ${props => props.theme.textTitleColor};

`

export const StyledBody = styled.Text`
  color: ${props => props.theme.textBodyColor};
  font-family: 'Montserrat_400Regular'
`

export const StyledTime = styled.Text`
  font-size: 14px;
  font-family: 'Montserrat_400Regular_Italic';
  color: ${props => props.theme.textTimeColor};
`

export const TitleTextInput = styled.TextInput.attrs(props => ({
  placeholderTextColor: props.theme.textPlaceholderColor,
  multiline: true,
  placeholder: 'Title'
}))`
  color: ${props => props.theme.textTitleColor}
  font-size: 22px;
  margin-bottom: 20px;
  font-family: 'Montserrat_600SemiBold'`;

export const BodyTextInput = styled.TextInput.attrs(props => ({
  placeholderTextColor: props.theme.textPlaceholderColor,
  multiline: true,
  placeholder: 'Type something...',
}))`
  color: ${props => props.theme.textBodyColor}
  font-size: 18px;
  font-family: 'Montserrat_400Regular';
  text-align-vertical: top;
`;