import * as React from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import NotesScreen from './screens/notes/NotesScreen';
import CreateNoteScreen from './screens/notes/NewNoteScreen';
import ViewNoteScreen from './screens/notes/ViewNoteScreen';
import FavoriteScreen from './screens/favorite/FavoriteScreen';
import SettingsScreen from './screens/notes/SettingsScreen';
import ArchivedScreen from './screens/archived/ArchivedScreen';
import { useTheme } from './themes'

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function NotesStack() {
  const theme = useTheme()
  return (
    <Stack.Navigator
      initialRouteName="Notes"
      screenOptions={{
        headerTitleAlign: 'flex-start',
        headerTintColor: theme.mode == 'dark' ? 'white' : 'black',
      }}
    >
      <Stack.Screen
        name="Notes"
        component={NotesScreen}
        options={{
          title: 'Notes',
          headerTitleStyle: {
            fontSize: 24,
            fontFamily: 'Montserrat_400Regular'
          }
        }}
      />
      <Stack.Screen
        name="CreateNote"
        component={CreateNoteScreen}
        options={{
          title: 'New Note',
          headerTitleAlign: 'center',
          headerTitleStyle: {
            fontFamily: 'Montserrat_400Regular'
          }
        }}
      />
      <Stack.Screen
        name="ViewNote"
        component={ViewNoteScreen}
        options={{
          headerTitleAlign: 'center',
          headerBackTitleVisible: true,
          headerBackTitleStyle: {
            fontFamily: 'Montserrat_400Regular'
          }
        }}
      />
      <Stack.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          title: 'Settings',
          headerTitleAlign: 'center',
          headerTitleStyle: {

            fontFamily: 'Montserrat_400Regular'
          }
        }}
      />
    </Stack.Navigator>
  );
}

function FavoriteStack() {
  const theme = useTheme()
  return (
    <Stack.Navigator
      initialRouteName="Favorite"
      screenOptions={{
        headerTitleAlign: 'flex-start',
        headerTintColor: theme.mode == 'dark' ? 'white' : 'black',
      }}>
      <Stack.Screen
        name="Favorite"
        component={FavoriteScreen}
        options={{
          title: 'Favorites',
          headerTitleStyle: {
            fontSize: 24,
            fontFamily: 'Montserrat_400Regular'
          }
        }}
      />
      <Stack.Screen
        name="ViewNote"
        component={ViewNoteScreen}
        options={{
          headerTitleAlign: 'center',
          headerBackTitleStyle: {
            fontFamily: 'Montserrat_400Regular'
          }
        }}
      />
    </Stack.Navigator>
  );
}

function ArchivedStack() {
  const theme = useTheme()
  return (
    <Stack.Navigator
      initialRouteName="Archived"
      screenOptions={{
        headerTitleAlign: 'flex-start',
        headerTintColor: theme.mode == 'dark' ? 'white' : 'black',
      }}
    >
      <Stack.Screen
        name="Archived"
        component={ArchivedScreen}
        options={{
          title: 'Archived',
          headerTitleStyle: {
            fontSize: 24,
            fontFamily: 'Montserrat_400Regular'
          }
        }}
      />
      <Stack.Screen
        name="ViewNote"
        component={ViewNoteScreen}
        options={{
          headerTitleAlign: 'center',
          headerBackTitleStyle: {
            fontFamily: 'Montserrat_400Regular'
          }
        }}
      />
    </Stack.Navigator>
  );
}

export default function Route() {
  const theme = useTheme()
  return (
    <NavigationContainer theme={theme.mode == 'dark' ? DarkTheme : DefaultTheme}>
      <Tab.Navigator
        initialRouteName="Feed"
        screenOptions={{
          headerShown: false
        }}>
        <Tab.Screen
          name="NotesStack"
          component={NotesStack}
          options={{
            tabBarLabel: 'Notes',
            tabBarActiveTintColor: theme.mode == 'dark' ? 'white' : 'black',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="note-multiple" color={color} size={size} />
            ),
          }}
        />
        <Tab.Screen
          name="FavoriteStack"
          component={FavoriteStack}
          options={{
            tabBarLabel: 'Favorite',
            tabBarActiveTintColor: 'red',
            tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons
                name="heart"
                color={color}
                size={size}
              />
            ),
          }}
        />
        <Tab.Screen
          name="ArchivedStack"
          component={ArchivedStack}
          options={{
            tabBarLabel: 'Archived',
            tabBarActiveTintColor: theme.mode == 'dark' ? 'white' : 'black',
            tabBarIcon: ({ color, size }) => (
              <MaterialIcons
                name="auto-delete"
                color={color}
                size={size}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
