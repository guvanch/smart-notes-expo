import * as React from 'react';
import { useState, useEffect } from 'react'
import {
  FlatList,
  View,
  TouchableOpacity,
  SafeAreaView
} from 'react-native';
import NotePreview from '../../components/NotePreview'
import { StyledView } from '../../components/Styled'
import NoteFound from '../../components/NotFound';
import { getArchived } from '../../service'

const ArchivedScreen = ({ navigation }) => {

  const [notes, setNotes] = useState([])

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getData()
    });
    return unsubscribe;
  }, [navigation])

  const getData = async () => {
    const data = await getArchived()
    setNotes(data)
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StyledView>
        <View
          style={{
            flex: 1
          }}>
          {notes.length > 0 ?
            <FlatList
              contentContainerStyle={{ paddingBottom: 60 }}
              data={notes}
              keyExtractor={item => item.id.toString()}
              renderItem={({ item }) =>
                <TouchableOpacity onPress={() =>
                  navigation.navigate('ViewNote', { note: item })
                }>
                  <NotePreview props={item} />
                </TouchableOpacity>
              }
            /> : <NoteFound />}
        </View>
      </StyledView>
    </SafeAreaView>
  );
};

export default ArchivedScreen;
