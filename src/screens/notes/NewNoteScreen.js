import * as React from 'react';
import { useState, useEffect } from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView, KeyboardAvoidingView, Platform, Button
} from 'react-native';
import { StyledView, TitleTextInput, BodyTextInput } from '../../components/Styled'
import { create } from '../../service'

const NewNoteScreen = ({ navigation }) => {

  const [title, setTitle] = useState('')
  const [body, setBody] = useState('')

  const setData = async () => {
    await create(title, body)
    navigation.goBack()
  }

  useEffect(() => {
    const unsubscribe = navigation.setOptions({
      headerRight: () => (
        <View style={styles.row}>
          <Button title="Save" disabled={title == '' || body == ''} onPress={() => { setData() }} />
        </View>
      ),
    });
    return unsubscribe
  }, [navigation, title, body]);

  const keyboardVerticalOffset = Platform.OS === 'ios' ? 100 : 0

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        keyboardVerticalOffset={keyboardVerticalOffset}
        style={{ flex: 1 }}
      >
        <StyledView style={{ padding: 16 }}>
          <TitleTextInput
            onChangeText={(val) => setTitle(val)}
            value={title}
          />
          <BodyTextInput
            numberOfLines={30}
            onChangeText={(val) => setBody(val)}
            value={body}
          />
        </StyledView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 20
  },
})

export default NewNoteScreen;
