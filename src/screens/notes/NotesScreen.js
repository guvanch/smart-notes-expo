import * as React from 'react';
import { useState, useEffect, useLayoutEffect } from 'react'
import {
  FlatList,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView
} from 'react-native';
import NotePreview from '../../components/NotePreview'
import { useTheme } from '../../themes'
import { StyledView } from '../../components/Styled'
import { FontAwesome, Ionicons } from '@expo/vector-icons';
import NoteFound from '../../components/NotFound'
import { getAll } from '../../service'

const NotesScreen = ({ navigation }) => {

  const [notes, setNotes] = useState([])
  const theme = useTheme()

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getData()
    });
    return unsubscribe;
  }, [navigation])

  useLayoutEffect(() => {
    const unsubscribe = navigation.setOptions({
      headerRight: () => headerRight(),
    });
    unsubscribe
  }, [navigation, theme]);

  const getData = async () => {
    const data = await getAll()
    setNotes(data)
  }

  const headerRight = () => {
    return (<FontAwesome name={'cog'} style={[styles.headerRight, { color: theme.mode == 'dark' ? '#cfcfcf' : 'grey' }]} size={24} onPress={() => { navigation.navigate('Settings') }} />)
  }

  const renderItem = (item) => {
    return (
      <TouchableOpacity activeOpacity={1} onPress={() =>
        navigation.navigate('ViewNote', { note: item })
      } style={styles.rowFront}>
        <NotePreview props={item} />
      </TouchableOpacity>
    )
  }

  return (
    <SafeAreaView style={styles.container}>
      <StyledView style={{
      }}>
        {notes.length > 0 ?
          <FlatList
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{ paddingBottom: 60 }}
            data={notes}
            keyExtractor={item => item.id.toString()}
            renderItem={({ item }) => renderItem(item)}
          />
          : <NoteFound />}
      </StyledView>
      <Ionicons style={[{ shadowColor: theme.mode == 'dark' ? 'white' : 'black' }, styles.floatingButton]} name="add-circle-sharp" size={50} onPress={() => { navigation.navigate('CreateNote') }} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  headerRight: {
    marginHorizontal: 20
  },
  floatingButton: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    shadowOpacity: 0.3,
    shadowRadius: 5,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOffset: {
      width: 0,
      height: 1,
    },
  }
});
export default NotesScreen;
