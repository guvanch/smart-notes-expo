import * as React from 'react';
import { useTheme } from '../../themes'
import {
  View,
  StyleSheet,
  SafeAreaView, Switch
} from 'react-native';
import { StyledView, StyledText } from '../../components/Styled'

const SettingsScreen = () => {

  const theme = useTheme()

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <StyledView>
        <View style={styles.row}>
          <StyledText style={{ fontFamily: 'Montserrat_400Regular', fontSize: 20 }}>Theme</StyledText>
          <Switch
            onTintColor="black"
            trackColor={{ false: theme.mode === 'dark' ? "black" : "grey", true: theme.mode === 'dark' ? "grey" : "black" }}
            thumbColor={theme.mode === 'dark' ? "black" : "white"}
            ios_backgroundColor={theme.mode === 'dark' ? "white" : "black"}
            onValueChange={value => theme.setMode(value ? 'dark' : 'light')}
            value={theme.mode === 'dark'}
            style={{ transform: [{ scaleX: .7 }, { scaleY: .7 }] }}
          />
        </View>
      </StyledView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  row: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 0.5,
    borderBottomColor: '#E5E3E2',
    padding: 10,
  },
  label: {
    fontSize: 16
  }
})

export default SettingsScreen;
