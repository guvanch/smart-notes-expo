import * as React from 'react';
import { useState, useEffect, useRef } from 'react';
import {
  StyleSheet,
  View,
  SafeAreaView, Button, KeyboardAvoidingView, Platform
  , TouchableWithoutFeedback, Keyboard
} from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import { StyledView, TitleTextInput, BodyTextInput, StyledTime, StyledText } from '../../components/Styled'
import Dates from '../../utils/dates'
import { useTheme } from '../../themes'
import { Menu, MenuItem } from 'react-native-material-menu';
import { setArchived, setFavorite, update } from '../../service'

const ViewNoteScreen = ({ route, navigation }) => {
  const note = route.params.note
  const [title, setTitle] = useState('')
  const [body, setBody] = useState('')
  const [liked, setLiked] = useState(0)

  const theme = useTheme()
  const menu = useRef();

  const hideMenu = () => menu.current.hide();
  const showMenu = () => menu.current.show();

  const saveData = async () => {
    await update(title, body, note.id)
    navigation.goBack()
  }

  const onFavorite = async (value) => {
    setLiked(value)
    await setFavorite(value, note.id)
    navigation.goBack()
    hideMenu()
  }

  const setArchive = async () => {
    const action = note.is_archived == 1 ? 0 : 1
    await setArchived(action, note.id)
    navigation.goBack()
    hideMenu()
  }

  useEffect(() => {
    const unsubscribe = setValues()
    return unsubscribe
  }, [])

  const setValues = () => {
    setTitle(note.title)
    setBody(note.body)
    setLiked(note.is_favorite)
  }

  useEffect(() => {
    const unsubscribe = navigation.setOptions({
      headerTitle: () => headerTitle(),
      headerRight: () => headerRight(),
    });
    return unsubscribe
  }, [navigation, title, body, liked]);

  const headerTitle = () => {
    return (
      <StyledTime>{Dates.currentDate(note.updated_at)}</StyledTime>
    )
  }

  const headerRight = () => {
    var isDisabled = (note.title == title && note.body == body) || (title == '' || body == '')
    return (
      <View style={styles.row}>
        <FontAwesome name='ellipsis-h' size={20} onPress={() => showMenu()} color={theme.mode == 'dark' ? 'white' : 'black'} style={{ marginHorizontal: 20 }} />
        <View style={{ marginRight: 10 }} >
          <Button title="Save" disabled={isDisabled} onPress={() => { saveData() }} />
        </View>
        <Menu ref={menu} button={<StyledText onPress={showMenu}>Show menu</StyledText>} onRequestClose={hideMenu}>
          <MenuItem onPress={() => onFavorite(note.is_favorite == 1 ? 0 : 1)}>{note.is_favorite == 1 ? "Remove from Favorites" : "Mark as Favorite"}</MenuItem>
          <MenuItem onPress={setArchive}>{note.is_archived == 1 ? "Unarchive" : "Archive"}</MenuItem>
        </Menu>
      </View>
    )
  }

  const keyboardVerticalOffset = Platform.OS === 'ios' ? 0 : 100

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        keyboardVerticalOffset={keyboardVerticalOffset}
        style={{ flex: 1 }}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <StyledView style={{ padding: 16 }}>
            <TitleTextInput
              autoCorrect={false}
              onChangeText={(val) => setTitle(val)}
              value={title}
            />

            <BodyTextInput
              numberOfLines={30}
              onChangeText={(val) => setBody(val)}
              value={body}
              style={{ paddingBottom: Platform.OS === "ios" ? 200 : 20 }}
            />
          </StyledView>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    width: 300,
    marginTop: 16,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonStyle: {
    marginHorizontal: 30
  }
})

export default ViewNoteScreen;
