import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase("db.db");

export async function createNewTable() {
  const query = "CREATE TABLE IF NOT EXISTS "
    + "Notes "
    + "(id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, body TEXT, created_at TEXT, updated_at TEXT, is_favorite BOOLEAN, is_archived BOOLEAN);"
  return new Promise((resolve, reject) => db.transaction(tx => {
    tx.executeSql(query, [], (_, result) => resolve(result), reject)
  }))
}

export async function getAll() {
  const query = "SELECT * FROM Notes WHERE is_archived=0 ORDER BY updated_at DESC;"
  return new Promise((resolve, reject) => db.transaction(tx => {
    tx.executeSql(query, [], (_, { rows }) => resolve(rows._array), reject)
  }))
}

export async function getFavorites() {
  const query = "SELECT * FROM Notes WHERE is_favorite=1 AND is_archived=0 ORDER BY updated_at DESC;"
  return new Promise((resolve, reject) => db.transaction(tx => {
    tx.executeSql(query, [], (_, { rows }) => resolve(rows._array), reject)
  }))
}

export async function getArchived() {
  const query = "SELECT * FROM Notes WHERE is_archived=1 ORDER BY updated_at DESC;"
  return new Promise((resolve, reject) => db.transaction(tx => {
    tx.executeSql(query, [], (_, { rows }) => resolve(rows._array), reject)
  }))
}

export async function setArchived(action, id) {
  const query = "UPDATE Notes SET is_archived=? WHERE id=?"
  return new Promise((resolve, reject) => db.transaction(tx => {
    tx.executeSql(query, [action, id], (_, result) => resolve(result), reject)
  }))
}

export async function setFavorite(action, id) {
  const query = "UPDATE Notes SET is_favorite=? WHERE id=?"
  return new Promise((resolve, reject) => db.transaction(tx => {
    tx.executeSql(query, [action, id], (_, result) => resolve(result), reject)
  }))
}

export async function create(title, body) {
  const now = Date.now()
  const query = "INSERT INTO Notes (title, body, created_at, updated_at, is_favorite, is_archived) VALUES  (?,?,?,?,?,?)"
  return new Promise((resolve, reject) => db.transaction(tx => {
    tx.executeSql(query, [title, body, now, now, false, false], (_, result) => resolve(result), reject)
  }))
}

export async function update(title, body, id) {
  const now = Date.now()
  const query = "UPDATE Notes SET title=?, body=?, updated_at=? WHERE id=?"
  return new Promise((resolve, reject) => db.transaction(tx => {
    tx.executeSql(query, [title, body, now, id], (_, result) => resolve(result), reject)
  }))
}