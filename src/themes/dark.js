const dark = {
  theme: {
    background: '#2E3440',
    border: '#575c66',
    backgroundAlt: '#575c66',
    borderAlt: '#2E3440',
    text: '#ECEFF4',
    textTitleColor: '#FFFFFF',
    textBodyColor: '#FFFFFF',
    textTimeColor: 'grey',
    textPlaceholderColor: '#C1C0C0'
  }
}

export default dark