const light = {
  theme: {
    background: '#ededed',
    border: '#bdbdbd',
    backgroundAlt: 'white',
    borderAlt: '#bdbdbd',
    text: '#171717',
    textTitleColor: '#000000',
    textBodyColor: '#000000',
    textTimeColor: 'grey',
    textPlaceholderColor: '#C1C0C0'
  }
}

export default light