const Dates = {
  timeSince(payload) {
    const time = parseFloat(payload)
    const date = new Date(time)
    const seconds = Math.floor((new Date() - date) / 1000)

    let interval = seconds / 31536000

    if (interval > 1) {
      return Math.floor(interval) + ' years ago'
    }

    interval = seconds / 2592000
    if (interval > 1) {
      return Math.floor(interval) + ' months ago'
    }

    interval = seconds / 86400
    if (interval > 1) {
      return Math.floor(interval) + ' days ago'
    }

    interval = seconds / 3600
    if (interval > 1) {
      return Math.floor(interval) + ' hours ago'
    }

    interval = seconds / 60
    if (interval > 1) {
      return Math.floor(interval) + ' minutes ago'
    }
    return 'just now'
  },
  currentDate(payload) {
    let timestamp = parseFloat(payload)
    let date = new Date(timestamp);
    return date.getDate() +
      "/" + (date.getMonth() + 1) +
      "/" + date.getFullYear()
  }
}

export default Dates